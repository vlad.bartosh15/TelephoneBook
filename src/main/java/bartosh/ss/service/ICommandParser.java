package bartosh.ss.service;

public interface ICommandParser {
    Commands getCommand(String str);

    String getBookId(String str);

    String getName(String str);

    String getTelephone(String str);
}
