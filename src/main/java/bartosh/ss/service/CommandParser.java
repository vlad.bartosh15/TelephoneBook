package bartosh.ss.service;

import lombok.Data;

@Data
public class CommandParser implements ICommandParser {
    private String[] partsStr;
    public Commands getCommand(String str){
        partsStr = str.split(" ");
        return Commands.valueOf(partsStr[0].toUpperCase());
    }
    public String getBookId(String str){
        partsStr = str.split(" ");
        return partsStr[1];
    }
    public String getName(String str){
        partsStr = str.split(" ");
        String result = partsStr[2] + " " + partsStr[3];
        return result;
    }
    public String getTelephone(String str){
        partsStr = str.split(" ");
        return partsStr[5];
    }
}
