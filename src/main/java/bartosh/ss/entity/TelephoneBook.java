package bartosh.ss.entity;

import lombok.Data;

import java.util.List;

@Data
public class TelephoneBook {
    private Long id;
    private List<Person> personList;
}
