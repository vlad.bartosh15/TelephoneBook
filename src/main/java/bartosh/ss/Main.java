package bartosh.ss;

import bartosh.ss.service.CommandParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {

            System.out.print("Enter something : ");
            String input = scanner.nextLine();
            if ("q".equals(input)) {
                System.out.println("Exit!");
                break;
            }
            CommandParser parser = new CommandParser();
            System.out.println("input : " + input);
            System.out.println("name : " + parser.getName(input));
            System.out.println("-----------\n");
        }

        scanner.close();

    }
}
